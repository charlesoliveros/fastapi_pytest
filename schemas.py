from typing import Optional
from pydantic import BaseModel


class ItemSchema(BaseModel):
    id: str
    title: str
    description: Optional[str] = None
