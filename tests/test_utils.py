import pytest
from utils import my_sum, write_file, Calculator


def test_my_sum_is_correctly():
    assert my_sum(3, 2) == 5


@pytest.mark.parametrize(
    'input_a, input_b, expected',
    [
        (3, 2, 5),
        (2, 3, 5),
        (my_sum(3, 2), 5, 10),
        (5, my_sum(3, 2), 10)
    ]
)
def test_my_sum_is_correctly_multi(input_a, input_b, expected):
    '''
    Test the same function with several parametrized values
    '''
    assert my_sum(input_a, input_b) == expected


def test_temp_dir(tmpdir):
    ''' tmpdir is the default name for temp dirs in pytest
    '''
    data_in = 'charles is writing'
    fpath = f'{tmpdir}/test.txt'

    write_file(fpath, data_in)

    with open(fpath) as file_out:
        data_out = file_out.read()

    assert data_in == data_out


def test_calculator_my_sum_is_always_four(monkeypatch):
    ''' monkeypatch is the variable's default name to mock specific results
    '''
    monkeypatch.setattr(Calculator, 'my_sum', lambda self, x, y: 4)
    calc = Calculator()
    assert calc.my_sum(2, 5) == 4
